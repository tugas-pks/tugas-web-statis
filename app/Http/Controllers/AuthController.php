<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function halo(Request $request)
    {
        $firstName = $request->input('firstName');
        $lastName = $request->input('lastName');
        return view('halo', compact('firstName', 'lastName'));
    }
}