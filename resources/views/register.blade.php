<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Account Baru</title>
</head>
<body>
    <div class="container-form">
        <header>
            <h1>Buat Account Baru</h1>
            <h2>Sign Up Form</h2>
        </header>
        <form action="{{ url('/welcome') }}" method="post">
            @csrf
            <label for="firstName">First name</label><br /><br />
            <input type="text" name="firstName" required /><br /><br />
            <label for="lastName">Last name</label><br /><br />
            <input type="text" name="lastName" required /><br /><br />
            <label for="gender">Gender</label><br /><br />
            <input type="radio" name="gender" value="male" required id="male" />Male<br />
            <input type="radio" name="gender" value="female" required id="female" />Female <br /><br />
            <label for="nationality">Nationality</label><br /><br />
            <select name="nationality" id="nationality" required>
                <option value="Indonesia">Indonesia</option>
                <option value="Inggris">Inggris</option>
            </select>
            <br /><br />
            <label for="languages">Language Spoken</label><br /><br />
            <input type="checkbox" name="languages[]" value="Bahasa Indonesia" required id="bahasa-indonesia" /> Bahasa Indonesia <br />
            <input type="checkbox" name="languages[]" value="English" id="english" /> English <br />
            <input type="checkbox" name="languages[]" value="Other" id="other" /> Other <br /><br />
            <label for="bio">Bio</label><br /><br />
            <textarea name="bio" id="bio" cols="30" rows="10" required></textarea>
            <br />
            <input type="submit" value="Submit" />
        </form>
    </div>
</body>
</html>