<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Media Online</title>
</head>
<body>
    <div class="container">
        <header>
            <h1>Media Online</h1>
        </header>
        <div class="sub-header">
            <h2>Social Media Developer</h2>
            <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
        </div>
        <div class="content">
            <h2>Benefit Join di Media Online</h2>
            <ul>
                <li>Mendapatkan motivasi dari sesama para Developer</li>
                <li>Sharing knowledge</li>
                <li></li>
            </ul>
            <h2>Cara Bergabung ke Media Online</h2>
            <ol>
                <li>Mengunjungi Website Ini</li>
                <li>Mendaftarkan di <a href="{{ url('/register') }}">Form Sign Up</a></li>
            </ol>
        </div>
    </div>
</body>
</html>